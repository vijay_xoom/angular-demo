var demo_app = angular.module('demo', []);
demo_app.controller('demo_ctrl', function( $scope ) {
	$scope.website_name = "Angular Demo";
	$scope.demo_text_1 = "Angular Demo 1 Text";
});
var demo_app_1 = angular.module('demo1', []);
demo_app_1.controller('demo_ctrl1', function( $scope ) {
	$scope.demo_text_2 = "Angular Demo 2 Text";
});
angular.bootstrap(document.getElementById("second-demo"), ['demo1']);